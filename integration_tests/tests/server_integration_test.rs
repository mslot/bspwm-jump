use std::{io::{BufReader, Read, Write}, net::{Shutdown}, sync::Arc, thread, time};
use bytes::{BytesMut, Buf, BufMut, Bytes};
use jump_lib::messages::{self, Command, CommandType, Request};
use jump_server::tcp::JumpServerHost;
use tokio::{io::AsyncReadExt, net::{TcpListener, TcpStream}};

#[tokio::test(flavor = "multi_thread")]
async fn host_start_stop() {
    let address = "127.0.0.1:45353";
    let host = Arc::new(JumpServerHost::new(address));

    let host_clone = host.clone();
    let start_handle = tokio::spawn(async move {
        host_clone.start_and_bind().await;
    });

    let host_clone = host.clone();
    let stop_handle = tokio::spawn(async move {
        host_clone.stop().await;
    });

    futures::try_join!(start_handle, stop_handle).unwrap();
}

#[tokio::test(flavor = "multi_thread")]
async fn send_list_command_receive_list() {
    let address = "127.0.0.1:45355";

    let listener = TcpListener::bind(address).await.unwrap();
    let host = Arc::new(JumpServerHost::new(address));

    let host_clone = host.clone();
    let start_handle = tokio::spawn(async move {
        host_clone.start(listener).await;
    });

    let mut request = Request::new();
    request.set_command(Command::COMMAND_WINOW);
    request.set_commandType(CommandType::COMMAND_TYPE_GET);

    println!("starting to connect so we can get response ...");
    let mut stream = TcpStream::connect(address).await.unwrap();
    println!("connected, beginning to create request ...");

    let mut bytes = BytesMut::with_capacity(1024);
    stream.read_buf(&mut bytes).await.unwrap();

    host.stop().await;
    start_handle.await.unwrap();
}
