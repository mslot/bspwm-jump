pub struct WindowNode {
    geometry: Geometry,
    window_information: WindowInformation,
}

impl WindowNode {
    pub fn new(
        title: &str,
        x: i32,
        y: i32,
        width: i32,
        height: i32,
        desktop_id: i32,
        window_id: &str,
    ) -> WindowNode {
        WindowNode {
            geometry: Geometry::new(x, y, width, height),
            window_information: WindowInformation::new(title, desktop_id, window_id),
        }
    }

    pub fn x(&self) -> i32 {
        self.geometry.x
    }

    pub fn y(&self) -> i32 {
        self.geometry.y
    }

    pub fn width(&self) -> i32 {
        self.geometry.width
    }

    pub fn height(&self) -> i32 {
        self.geometry.height
    }

    pub fn desktop_id(&self) -> i32 {
        self.window_information.desktop_id
    }

    pub fn window_id(&self) -> &str {
        self.window_information.window_id.as_str()
    }

    pub fn title(&self) -> &str {
        self.window_information.title.as_str()
    }
}

pub struct WindowInformation {
    title: String,
    desktop_id: i32,
    window_id: String,
}

impl WindowInformation {
    pub fn new(title: &str, desktop_id: i32, window_id: &str) -> WindowInformation {
        WindowInformation {
            title: String::from(title),
            desktop_id,
            window_id: String::from(window_id),
        }
    }
}

pub struct Geometry {
    x: i32,
    y: i32,
    height: i32,
    width: i32,
}

impl Geometry {
    pub fn new(x: i32, y: i32, width: i32, height: i32) -> Geometry {
        Geometry {
            x,
            y,
            width,
            height,
        }
    }
}
